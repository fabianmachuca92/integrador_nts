﻿using Contracts.Repository.Base;
using Contracts.Services;
using Entities.Models.Auth;
using System.Linq;

namespace Services
{
    public class AuthServices : IAuthServices
    {
        protected IRepositoryWrapper _repoWrapper;
        public AuthServices(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public User LogIn(string UserName, string SecretCode)
        {
            var _user=_repoWrapper.User.ExecuteStoredProcedure("se llama al sp que valida el usuario y clave, le paso parametros").FirstOrDefault() ;
            
            return _user;

        }
    }
}