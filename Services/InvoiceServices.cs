﻿using Contracts.Repository.Base;
using Contracts.Services;
using Entities;
using Entities.Models.ViewModelRequest;
using Entities.ViewModelResponse;
using System.Linq;

namespace Services
{
    public class InvoiceServices : ITransaccionServices
    {

        protected IRepositoryWrapper _repoWrapper;
        public InvoiceServices(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }


        public ccTransaccionViewModelResponse InsertTransaccion_V1(ccTransaccionViewModelRequest_V1 transaccion)
        {

            /*
             se realizan validaciones de negocio sobre el objeto recibido.

            que los valores coincidan
            que el cliente exista, si no existe se crea
            que envie los datos completos

            lo que tiene q hacer es crear los sp o utilizar los que ya existen enviandole los parametros que piden.
            Tal vez crearlo es mas rapido, es un insert directo a la tabla, no se valida nada.. todo se valida aqui.
             */

            /*
          
            */
            ccTransaccionViewModelResponse _response = new ccTransaccionViewModelResponse();
           


            _response = ValidarFactura(transaccion);

            if (_response.ciStatus == Constants.errorStatus)
                return _response;

            _repoWrapper.Transaccion.ExecuteStoredProcedure("aqui llamo al sp q va a insertar la cab y le paso parametros");
            _repoWrapper.TransaccionItem.ExecuteStoredProcedure("aqui llamo al sp q va a insertar la det y le paso parametros");
            _repoWrapper.Customer.ExecuteStoredProcedure("aqui llamo al sp q va a insertar la cliente y le paso parametros");
            Contabilizar(0);
            return null;
        }

        private ccTransaccionViewModelResponse ValidarFactura(ccTransaccionViewModelRequest_V1 transaccion)
        {
            ccTransaccionViewModelResponse _response = new ccTransaccionViewModelResponse();


            //preguntar a nancy x cada campo q se va a insertar si es necesario pedirlo o se "quema" ya que el bat tiene algunos campos que siempre va el mismo valor

            if (transaccion.ciTipoTransaccion != 1 || transaccion.ciTipoTransaccion == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El tipo de transaccion debe ser 1.");

            if (transaccion.ciDepartamento == 0 || transaccion.ciDepartamento == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar el departamento.");

            if (transaccion.ciLocalidad == 1 || transaccion.ciLocalidad == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar la localidad.");

            if (transaccion.ciAgencia == 1 || transaccion.ciAgencia == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar la agencoa.");

            if (string.IsNullOrEmpty(transaccion.txNumeroFactura))
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar el numero de factura.");

            if (string.IsNullOrEmpty(transaccion.txNumeroFactura))
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar la serie.");

            if (transaccion.vaTotal != (transaccion.vaIVA - transaccion.vaDescuento + transaccion.vaSubTotal))
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El total no coincide.");

            if (transaccion.Cliente == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El cliente es obligatorio.");
            else
            {
                if (string.IsNullOrEmpty(transaccion.Cliente.txIdentificacion))
                    _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "La identificación del cliente es obligatorio.");

                if (transaccion.Cliente.ciTipoCliente != 1)
                {
                    if (string.IsNullOrEmpty(transaccion.Cliente.txNombres))
                        _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El nombre del cliente es obligatorio.");

                    if (string.IsNullOrEmpty(transaccion.Cliente.txApellidos))
                        _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El apellido del cliente es obligatorio.");
                }
                else
                {
                    if (string.IsNullOrEmpty(transaccion.Cliente.txRazonSocial))
                        _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "La razón social del cliente es obligatorio.");
                }

                if (string.IsNullOrEmpty(transaccion.Cliente.ciCodAseguradoraEFI))
                    _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El código EFI es obligatorio.");

            }

            if (transaccion.TransaccionItems == null || transaccion.TransaccionItems.Count == 0)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "No se encontraron detalles de la factura.");
            else
            {

                foreach (var item in transaccion.TransaccionItems)
                {
                    if (string.IsNullOrEmpty(item.txDescripcionItem))
                        _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "La descripción del item es obligatorio.");
                    
                    if (item.vaTotal != item.vaIVA - item.vaDescuento + item.vaSubTotal)
                        _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El total en los items no coincide.");
                }

                if(transaccion.vaTotal!= transaccion.TransaccionItems.Sum(a=>a.vaTotal))
                    _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El total de los items con el total de la cabecera no coincide.");
            }


            return _response;

        }


        private ccTransaccionViewModelResponse ValidarNC(ccTransaccionViewModelRequest_V1 transaccion)
        {
            ccTransaccionViewModelResponse _response = new ccTransaccionViewModelResponse();


            //preguntar a nancy x cada campo q se va a insertar si es necesario pedirlo o se "quema" ya que el bat tiene algunos campos que siempre va el mismo valor

            if (transaccion.ciTipoTransaccion != 1 || transaccion.ciTipoTransaccion == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El tipo de transaccion debe ser 2.");

            if (transaccion.ciDepartamento == 0 || transaccion.ciDepartamento == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar el departamento.");

            if (transaccion.ciLocalidad == 1 || transaccion.ciLocalidad == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar la localidad.");

            if (transaccion.ciAgencia == 1 || transaccion.ciAgencia == null)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar la agencoa.");

            if (string.IsNullOrEmpty(transaccion.txNumeroFactura))
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar el numero de nota de credito.");

            if (string.IsNullOrEmpty(transaccion.txNumeroFactura))
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "Debe ingresar la serie.");

            if (transaccion.vaTotal != (transaccion.vaIVA - transaccion.vaDescuento + transaccion.vaSubTotal))
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El total no coincide.");

            

            if (transaccion.TransaccionItems == null || transaccion.TransaccionItems.Count == 0)
                _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "No se encontraron detalles de la nota de crédito.");
            else
            {

                foreach (var item in transaccion.TransaccionItems)
                {
                    if (string.IsNullOrEmpty(item.txDescripcionItem))
                        _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "La descripción del item es obligatorio.");

                    if (item.vaTotal != item.vaIVA - item.vaDescuento + item.vaSubTotal)
                        _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El total en los items no coincide.");
                }

                if (transaccion.vaTotal != transaccion.TransaccionItems.Sum(a => a.vaTotal))
                    _response.txMessage = string.Format("{0} {1} ", _response.txMessage, "El total de los items con el total de la cabecera no coincide.");
            }

            
            return _response;

        }


        public ccTransaccionViewModelResponse InsertNotaCredito_V1(ccTransaccionViewModelRequest_V1 transaccion)
        {

            /*
             La nota de credito se inserta en la misma tabla ccTransaccion solo que lleva un tipo diferente
            se debe validar que exista la transaccion a anular y cualquier otra validacion de negocio que diga nancy
             
            No se debe crear cliente

            lo que tiene q hacer es crear los sp o utilizar los que ya existen enviandole los parametros que piden
             */

            ccTransaccionViewModelResponse _response = new ccTransaccionViewModelResponse();



            _response = ValidarNC(transaccion);

            if (_response.ciStatus == Constants.errorStatus)
                return _response;

            _repoWrapper.Transaccion.ExecuteStoredProcedure("aqui llamo al sp q va a insertar la cab y le paso parametros");
            _repoWrapper.TransaccionItem.ExecuteStoredProcedure("aqui llamo al sp q va a insertar la det y le paso parametros");
            Contabilizar(0);
            return null;
        }


        private bool Contabilizar(int ciTransaccion)
        {
            _repoWrapper.Customer.ExecuteStoredProcedure("aqui llama al sp q contabiliza y le pasa los parametros", ciTransaccion);
            return true;
        }
    }
}