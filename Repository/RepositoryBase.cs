﻿using Context;
using Contracts.Repository.Base;
using Entities;
using Entities.Interface.Models;
using Entities.Models.Auth;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;

namespace Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly FEContext _repositoryContext;


        public RepositoryBase(FEContext repositoryContext)
        {
            _repositoryContext = repositoryContext;
        }

        private DbSet<T> AsDbSet()
        {
            return _repositoryContext.Set<T>();
        }

        public IList<T> ExecuteStoredProcedure(string nameStoredProcedure, params object[] parameters)
        {
            ///revisar este link https://referbruv.com/blog/posts/working-with-stored-procedures-in-aspnet-core-ef-core

            var _query = AsDbSet();
            return _query.FromSqlRaw(nameStoredProcedure, parameters).ToList();
        }


    }

}