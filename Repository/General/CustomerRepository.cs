﻿using Context;
using Contracts.Repository.General;
using Entities.Models.General;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.General
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(FEContext repositoryContext)
            : base(repositoryContext)
        {
        }
    
    }
}
