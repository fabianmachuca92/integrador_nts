﻿using Context;
using Contracts.Repository.General;
using Entities.Models.General;
using Microsoft.AspNetCore.Http;

namespace Repository.General
{
    public class CompanyRepository : RepositoryBase<Company>, ICompanyRepository
    {
        public CompanyRepository(FEContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}