﻿using Context;
using Contracts.Repository.Business;
using Entities.Models.Business;
using Microsoft.AspNetCore.Http;

namespace Repository.Business
{
    public class TransaccionItemRepository : RepositoryBase<ccTransaccionItem>, ITransaccionItemRepository
    {
        public TransaccionItemRepository(FEContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}