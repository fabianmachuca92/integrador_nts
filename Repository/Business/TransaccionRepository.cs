﻿using Context;
using Contracts.Repository.Business;
using Entities.Models.Business;
using Microsoft.AspNetCore.Http;

namespace Repository.Business
{
    public class TransaccionRepository : RepositoryBase<ccTransaccion>, ITransaccionRepository
    {
        public TransaccionRepository(FEContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}