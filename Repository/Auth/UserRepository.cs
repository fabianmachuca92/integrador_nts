﻿using Context;
using Contracts.Repository.General;
using Entities.Models.Auth;
using Entities.Models.Business;
using Entities.Models.General;
using Microsoft.AspNetCore.Http;

namespace Repository.Auth
{
    public class UserRepository : RepositoryBase<User>, IUserRepository
    {
        public UserRepository(FEContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}