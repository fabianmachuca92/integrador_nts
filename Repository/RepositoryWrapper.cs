﻿using Context;
using Contracts.Repository.Base;
using Contracts.Repository.Business;
using Contracts.Repository.General;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Repository.General;
using Repository.Auth;
using Repository.Business;

namespace Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private FEContext _feContext;

        private ICompanyRepository _company;
        private IUserRepository _user;
        private ITransaccionRepository _transaccion;
        private ITransaccionItemRepository _transaccionItem;
        private ICustomerRepository _customer;


        public IUserRepository User
        {
            get
            {
                if (_user == null)
                    _user = new UserRepository(_feContext);
                return _user;
            }
        }
        public ICompanyRepository Company
        {
            get
            {
                if (_company == null)
                    _company = new CompanyRepository(_feContext);
                return _company;
            }
        }

        public ITransaccionRepository Transaccion
        {
            get
            {
                if (_transaccion == null)
                    _transaccion = new TransaccionRepository(_feContext);
                return _transaccion;
            }
        }

        public ITransaccionItemRepository TransaccionItem
        {
            get
            {
                if (_transaccionItem == null)
                    _transaccionItem = new TransaccionItemRepository(_feContext);
                return _transaccionItem;
            }
        }

        public ICustomerRepository Customer
        {
            get
            {
                if (_customer == null)
                    _customer = new CustomerRepository(_feContext);
                return _customer;
            }
        }
    }
}