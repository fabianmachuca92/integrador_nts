﻿using Contracts.Repository.Base;
using Entities.Models.Auth;
using Entities.Models.General;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Repository.General
{
    public interface IUserRepository : IRepositoryBase<User>
    {
    }
   
}