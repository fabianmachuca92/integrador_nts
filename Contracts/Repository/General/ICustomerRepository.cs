﻿using Contracts.Repository.Base;
using Entities.Models.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Contracts.Repository.General
{
    public interface ICustomerRepository : IRepositoryBase<Customer>
    {

    }
}