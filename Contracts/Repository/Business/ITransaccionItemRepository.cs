﻿using Contracts.Repository.Base;
using Entities.Models.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Contracts.Repository.Business
{
    public interface ITransaccionItemRepository : IRepositoryBase<ccTransaccionItem>
    {

    }
}