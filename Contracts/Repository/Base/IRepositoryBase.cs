﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Contracts.Repository.Base
{
    public interface IRepositoryBase<T>
    {
        public IList<T> ExecuteStoredProcedure(string StoredProcedure, params object[] parameters);
    }
}