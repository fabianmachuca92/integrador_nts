﻿using Contracts.Repository.Business;
using Contracts.Repository.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml.Linq;

namespace Contracts.Repository.Base
{
    public interface IRepositoryWrapper
    {
        IUserRepository User { get; }
        ICompanyRepository Company { get; }

        ICustomerRepository Customer { get; }
        ITransaccionItemRepository TransaccionItem { get; }
        ITransaccionRepository Transaccion { get; }

    }
}