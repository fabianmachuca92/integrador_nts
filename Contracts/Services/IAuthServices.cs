﻿using Entities.Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using System.Xml.Linq;

namespace Contracts.Services
{
    public interface IAuthServices
    {
        public User LogIn(string UserName, string SecretCode );

    }
}