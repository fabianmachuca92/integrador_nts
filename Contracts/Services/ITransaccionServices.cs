﻿
using Entities.Models.ViewModelRequest;
using Entities.ViewModelResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using System.Xml.Linq;

namespace Contracts.Services
{
    public interface ITransaccionServices
    {
        
        
        ccTransaccionViewModelResponse InsertTransaccion_V1(ccTransaccionViewModelRequest_V1 transaccion);

        ccTransaccionViewModelResponse InsertNotaCredito_V1(ccTransaccionViewModelRequest_V1 transaccion);


    }
}