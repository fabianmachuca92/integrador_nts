﻿using Entities.Interface.Models;
using Entities.Models.Auth;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Text;

namespace Entities
{
    public static class ExtensionMethods
    {
     

        public static string Serializar(this IEntity entry, JsonSerializerSettings settings = null)
        {
            return JsonConvert.SerializeObject(entry, settings);
        }

        public static IEntry Deserialize<IEntry>(this string json) 
        {
            return JsonConvert.DeserializeObject<IEntry>(json);
        }


        public static decimal ToDecimalWithDecimalPoint(this string value)
        {
            return decimal.Parse(value, new NumberFormatInfo() { NumberDecimalSeparator = "." });
        }


    }

   

}