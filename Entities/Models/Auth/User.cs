﻿using Entities.Interface.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Text;

namespace Entities.Models.Auth
{

    public class User : IEntity
    {

        ///tiene que crear el sp que devuelva lo que va a mapear aqui
        ///en este caso el sp que valida el log in a partir de el usuario y la clave
        

        public string ciStatus { get; set; }
        public string txMessage { get; set; }
        public string txUserName { get; set; }
        public int ciCompanyId { get; set; }
        public string JwtBearer { get; set; }

    }
}