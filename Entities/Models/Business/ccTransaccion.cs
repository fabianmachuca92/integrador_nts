﻿using Entities.Interface.Models;
using Entities.Models.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models.Business
{
    public class ccTransaccion : IEntity
    {
        /// <summary>
        /// Dejar solo las propiedades necesarias para insertar en la tabla ccTransaccion
        /// Esto es un espejo de lo que va a necesitar para insertar o consultar
        /// 
        /// Debe verificar que no falten campos necesarios, esto lo debe confirmar con Nancy
        /// </summary>

        public int ciCompania { get; set; }
        public int ciTransaccion { get; set; }
        public int ciTipoTransaccion { get; set; }
        public string ciDepartamento { get; set; }
        public int ciLocalidad { get; set; }
        public int ciAgencia { get; set; }
        public string txNumeroFactura { get; set; }
        public int ciCliente { get; set; }
        public int ciContrato { get; set; }
        public int ciPlazo { get; set; }
        public int ciVendedor { get; set; }
        public string txReferencia { get; set; }
        public string ciMoneda { get; set; }
        public string txDescripcion { get; set; }
        public DateTime fcVenta { get; set; }
        public string txSerie { get; set; }
        public string txAutImprenta { get; set; }
        public string txAutContribuyente { get; set; }
        public int prIVA { get; set; }
        public decimal vaSubTotal { get; set; }
        public decimal vaDescuento { get; set; }
        public decimal vaIVA { get; set; }
        public string vaTotal { get; set; }
        public bool bdCredito { get; set; }
        public int txDias { get; set; }
        public decimal vaCobrado { get; set; }
        public decimal prRetFuente { get; set; }
        public decimal vaRetFuente { get; set; }
        public decimal udPorcentaje { get; set; }
        public decimal vaRetIVA { get; set; }
        public decimal vaIVACero { get; set; }
        public decimal vaIVADoce { get; set; }
        public int ciAsiento { get; set; }
        public int ciAsientoReverso { get; set; }
        public DateTime fcContabilizacion { get; set; }
        public DateTime fcCobro { get; set; }
        public DateTime fcIngreso { get; set; }
        public DateTime fcModificacion { get; set; }
        public string ciUsuarioIngreso { get; set; }
        public string ciUsuarioModifica { get; set; }
        public string ciEstado { get; set; }
        public string ciUsuarioAutoriza { get; set; }
        public string ciCodFacturaEFI { get; set; }
        public int ciTransaccionFactura { get; set; }
        public DateTime fcAutorizacion { get; set; }
        public string txXML { get; set; }
        public int vaPlazo { get; set; }
        public string txNumeroOrdenCompra { get; set; }
        public int ciFormaCobro { get; set; }
        public string txNumAutorizacion { get; set; }
        public string txClaveAcceso { get; set; }

        public virtual List<ccTransaccionItem> TransaccionItems { get; set; }
    }
}