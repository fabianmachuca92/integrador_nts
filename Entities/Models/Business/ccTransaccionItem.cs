﻿using Entities.Interface.Models;
using Entities.Models.General;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models.Business
{
    public class ccTransaccionItem : IEntity
    {
        /// <summary>
        /// Dejar solo las propiedades necesarias para insertar en la tabla ccTransaccion
        /// Esto es un espejo de lo que va a necesitar para insertar o consultar
        /// </summary>


        public int ciCompania { get; set; }
        public int ciTransaccion { get; set; }
        public int ciSecuencia { get; set; }
        public int ciItem { get; set; }
        public int qnCantidad { get; set; }
        public string ciReferencia { get; set; }
        public decimal vaValorUnitario { get; set; }
        public decimal vaDescuento { get; set; }
        public bool bIVA { get; set; }
        public decimal vaIVA { get; set; }
        public int ciSubcentro { get; set; }
        public decimal vaTotal { get; set; }
        public decimal vaSubTotal { get; set; }
        public decimal vaCobrado { get; set; }
        public decimal vaIVACobrado { get; set; }
        public decimal vaRetenido { get; set; }
        public decimal vaIVARetenido { get; set; }
        public string txDescripcionItem { get; set; }

        public int tipoPrecio { get; set; }
        public int ciBodega { get; set; }
        public int ciSubBodega { get; set; }
        public string txLote { get; set; }
        public decimal prIVA { get; set; }
    }
}