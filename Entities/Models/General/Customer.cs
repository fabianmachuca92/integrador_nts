﻿using Entities.Interface.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models.General
{
    public class Customer : IEntity
    {

        ///Aqui mapea la tabla cliente en el caso que necesite consultarla 
        ///tiene que crear el sp que devuelva lo que va a mapear aqui
        ///
        ///Tambien debe usar esta entidad para insertar los datos en el sp que va a grabar o actualizar los datos
        
        public int ciCompania { get; set; }
        public int ciCliente { get; set; }
        public int ciTipoIdentificacion { get; set; }
        public string txIdentificacion { get; set; }
        public int ciTipoPersona { get; set; }
        public int ciTipoCliente { get; set; }
        public int ciPlazo { get; set; }
        public int ciFormaCobro { get; set; }
        public string txNombres { get; set; }
        public string txApellidos { get; set; }
        public string txRazonSocial { get; set; }
        public string txNombreComercial { get; set; }
        public string txAdministrador { get; set; }
        public string txPropietario { get; set; }
        public string txDireccion { get; set; }
        public string txDireccionCobro { get; set; }
        public string txDireccionEntrega { get; set; }
        public string txEmail { get; set; }
        public string txWeb { get; set; }
        public decimal vaCobrado { get; set; }
        public decimal vaPorCobrar { get; set; }
        public decimal vaCupoDisponible { get; set; }
        public bool bdRetIva { get; set; }
        public string txReferencia { get; set; }
        public string txTelefono { get; set; }
        public DateTime fcIngreso { get; set; }
        public string ciUsuarioIngreso { get; set; }
        public DateTime fcModificacion { get; set; }
        public string ciUsuarioModifica { get; set; }
        public string ciEstado { get; set; }
        public string ciLocalidad { get; set; }
        public string ciCodAseguradoraEFI { get; set; }
        public string vaPlazo { get; set; }
        public int ciVendedor { get; set; }

    }
}