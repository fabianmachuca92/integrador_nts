﻿using Entities.Interface.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models.ViewModelRequest
{
    public class ccClienteViewModelRequestV1 : IEntity
    {

        /// <summary>
        /// Dejar solo las propiedades necesarias que se le va a pedir al usuario
        /// 
        /// Debe verificar que no falten campos necesarios, esto lo debe confirmar con Nancy y con esto
        /// llena el objeto Business correspondiente que va a la base
        /// </summary>
        
        public int ciTipoIdentificacion { get; set; }
        public string txIdentificacion { get; set; }
        public int ciTipoPersona { get; set; }
        public int ciTipoCliente { get; set; }
        public int ciPlazo { get; set; }
        public int ciFormaCobro { get; set; }
        public string txNombres { get; set; }
        public string txApellidos { get; set; }
        public string txRazonSocial { get; set; }
        public string txNombreComercial { get; set; }
        public string txAdministrador { get; set; }
        public string txPropietario { get; set; }
        public string txDireccion { get; set; }
        public string txDireccionCobro { get; set; }
        public string txDireccionEntrega { get; set; }
        public string txEmail { get; set; }
    
        public string ciLocalidad { get; set; }
        public string ciCodAseguradoraEFI { get; set; }
        public string vaPlazo { get; set; }
        public int ciVendedor { get; set; }

    }
}