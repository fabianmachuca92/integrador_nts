﻿using Entities.Interface.Models;
using Entities.Models.Auth;
using Entities.Others;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore.Http;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml;

namespace Entities
{


    public static class Helpers
    {
        private static log4net.ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static bool configLoadedLog4Net = false;
        public enum typeMessage
        {
            Error,
            Warning,
            Info,
            Debug

        }

        public static void RegisterMessageError(Exception ex)
        {
            if (!configLoadedLog4Net)
            {
                var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            }

            log.Error(ex);
        }

        public static void RegisterMessage(string message, typeMessage type)
        {
            if (!configLoadedLog4Net)
            {
                var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
                XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            }

            if (type == typeMessage.Error)
                log.Error(message);
            else
            {
                if (type == typeMessage.Warning)
                    log.Warn(message);
                else
                {
                    if (type == typeMessage.Info)
                        log.Info(message);
                    else
                    {
                        if (type == typeMessage.Debug)
                            log.Debug(message);
                        else
                            log.Fatal(message);
                    }
                }
            }
        }



    }

}