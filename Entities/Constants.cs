﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Constants
    {
        public const string activeStatus = "A";
        public const string inactiveStatus = "I";
        public const string errorStatus = "E";

    }
}