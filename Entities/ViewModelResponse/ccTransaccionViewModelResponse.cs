﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ViewModelResponse
{
    public class ccTransaccionViewModelResponse
    {
        public string ciStatus { get; set; }
        public string txMessage { get; set; }
        public int ciTransaccion { get; set; }
    }
}
