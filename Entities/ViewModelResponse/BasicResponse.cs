﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.ViewModelResponse
{
    public class BasicResponse
    {
        public string ciStatus { get; set; }
        public string txMessage { get; set; }
    }
}
