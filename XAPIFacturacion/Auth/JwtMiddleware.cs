﻿using Entities;
using Entities.Models.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace APIBAT.Auth
{


    public class JwtMiddleware
    {

        private IConfiguration _configuration { get; }
        private readonly RequestDelegate _next;

        public JwtMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            _configuration = configuration;
        }

        public async Task Invoke(HttpContext context)
        {
            var endpoint = context.GetEndpoint();
            if (endpoint?.Metadata?.GetMetadata<IAllowAnonymous>() is object)
            {
                await _next(context);
                return;
            }

            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                attachUserToContext(context, token);
            else
            {
                context.Response.StatusCode = 401; //UnAuthorized
                await context.Response.WriteAsync(new Entities.Others.UnAuthorized() { message = "Invalid authentication token" }.Serializar());
            }
            await _next(context);
        }

        private void attachUserToContext(HttpContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();

                tokenHandler.ValidateToken(token,
                     JWTParameters(
                         _configuration.GetValue<string>("Auth:SecretKey"),
                         _configuration.GetValue<string>("Auth:Issuer"),
                         _configuration.GetValue<string>("Auth:Audience"))
                     , out SecurityToken validatedToken);

            }
            catch
            {
                context.Response.StatusCode = 401; //UnAuthorized
                context.Response.WriteAsync(new Entities.Others.UnAuthorized() { message = "Invalid authentication token" }.Serializar());
            }
        }


        internal static TokenValidationParameters JWTParameters(string secretKey, string issuer, string audience)
        {
            var key = Encoding.ASCII.GetBytes(secretKey);
            return new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidIssuer = issuer,
                ValidAudience = audience
            };

        }
    }
}