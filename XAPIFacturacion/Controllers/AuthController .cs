﻿using Contracts.Services;
using Entities;
using Entities.Models.Auth;
using Entities.Others;
using Entities.ViewModelRequest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace APIBAT.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthServices _authServices;

        private readonly IConfiguration _configuration;

        public AuthController(IConfiguration configuration, IAuthServices authServices)
        {
            _configuration = configuration;
            _authServices = authServices;
        }


        [HttpGet]
        public IActionResult Login()
        {
            /// Este es un ejemplo de token para hacer pruebas en los metodos de autenticacion
            /// este codigo se debe comentar y dejar el siguiente código
            /// 
            /// return StatusCode( StatusCodes.Status405MethodNotAllowed, new User() { ciStatus = Constants.error_SRI, txMessage = "method not allowed." });

            // Asumamos que nos envia un usuario válido
            var user = new User
            {
                ciCompanyId = 1,
                txUserName = "fabian machuca",
            };

            // Leemos el secret_key desde nuestro appseting

            var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("Auth:SecretKey"));

            // Creamos los claims (pertenencias, características) del usuario

            var claims = new[]
            {
                new Claim(ClaimTypes.UserData, user.Serializar())
            };


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                // Nuestro token va a durar 5 años
                Expires = DateTime.UtcNow.AddYears(5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);

            user.JwtBearer = tokenHandler.WriteToken(createdToken);


            return Ok(user);
        }


        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var _user = _authServices.LogIn(model.UserName, model.SecretCode);
                    if (_user.ciStatus == Constants.activeStatus)
                    {
                        var key = Encoding.ASCII.GetBytes(_configuration.GetValue<string>("Auth:SecretKey"));

                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(new[]
                        {
                            new Claim(ClaimTypes.UserData, _user.Serializar())
                        }),
                            Expires = DateTime.UtcNow.AddYears(5),
                            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                        };

                        var tokenHandler = new JwtSecurityTokenHandler();
                        var createdToken = tokenHandler.CreateToken(tokenDescriptor);
                        _user.JwtBearer = tokenHandler.WriteToken(createdToken);
                        return Ok(_user);
                    }
                    else
                    {   
                        return StatusCode(StatusCodes.Status401Unauthorized, _user);
                    }

                }
                else
                {
                    var message = new User() { ciStatus = Constants.errorStatus, txMessage = "parameters were not received." };
                    foreach (var errors in ModelState.Values)
                        foreach (var error in errors.Errors)
                        {
                            message.txMessage = string.Format("{0} {1}", message.txMessage, error.ErrorMessage);
                        }
                    return BadRequest(message);

                }
            }
            catch (Exception ex)
            {
                Helpers.RegisterMessageError(ex);
                return StatusCode(StatusCodes.Status500InternalServerError, new User() { ciStatus = Constants.errorStatus, txMessage = "Internal server error." });
            }


        }

    }
}