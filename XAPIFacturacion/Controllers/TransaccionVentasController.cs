﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using System.Xml;
using Contracts;
using Contracts.Services;
using Entities;
using Entities.Models.ViewModelRequest;
using Entities.Others;
using Entities.ViewModelResponse;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services;

namespace APIBAT.Controllers
{
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public class TransaccionVentasController : ControllerBase
    {

        private readonly ITransaccionServices _transaccionServices;

        public TransaccionVentasController(ITransaccionServices invoiceServices)
        {
            _transaccionServices = invoiceServices;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return StatusCode(StatusCodes.Status405MethodNotAllowed, new BasicResponse() { ciStatus = Constants.errorStatus, txMessage = "method not allowed." });
        }

        [HttpGet("{ciTransaccion}", Name = "TransaccionVentas")]
        public IActionResult Get(string ciTransaccion)
        {
            return StatusCode(StatusCodes.Status405MethodNotAllowed, new BasicResponse() { ciStatus = Constants.errorStatus, txMessage = "method not allowed." });

        }

        [HttpPost]
        public IActionResult Post([FromForm] string txJson)
        {
            try
            {
                ///Aqui se recibe lo que envia el usuario, se debe insertar y validar en la capa de servicios
                ///esto a futuro va a deserealizar dependiendo la version que envia el usuario, por esa razon se pide
                ///un string y no el objeto como tal
                

                var _transaccion = txJson.Deserialize<ccTransaccionViewModelRequest_V1>();
                var response = _transaccionServices.InsertTransaccion_V1(_transaccion);
                
                if (response.ciStatus == Constants.activeStatus)
                  return Ok(response);
                else
                   return BadRequest(response);

            }
            catch (Exception ex)
            {
                Helpers.RegisterMessageError(ex);
                return StatusCode(StatusCodes.Status500InternalServerError, new BasicResponse() { ciStatus = Constants.errorStatus, txMessage = "Internal server error." });
            }

        }

        [HttpPut("{ciTransaccion}")]
        public IActionResult Put(string ciTransaccion, [FromForm] string txJson)
        {
            return StatusCode(StatusCodes.Status405MethodNotAllowed, new BasicResponse() { ciStatus = Constants.errorStatus, txMessage = "method not allowed." });
        }

        [HttpDelete]
        public IActionResult Delete()
        {
            return StatusCode(StatusCodes.Status405MethodNotAllowed, new BasicResponse() { ciStatus = Constants.errorStatus, txMessage = "method not allowed." });
        }

        [HttpPatch]
        public IActionResult Patch()
        {
            return StatusCode(StatusCodes.Status405MethodNotAllowed, new BasicResponse() { ciStatus = Constants.errorStatus, txMessage = "method not allowed." });
        }

    }
}