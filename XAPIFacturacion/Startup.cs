using APIBAT.Auth;
using Context;
using Contracts.Repository.Base;
using Contracts.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Repository;
using Services;

namespace APIBAT
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //services.ConfigureSqlServerContext(Configuration);
            services.AddHttpContextAccessor();
            services.AddDbContext<FEContext>(a => a.UseSqlServer(Configuration["SqlServerconection:connectionString"]));

            services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();
            services.AddScoped<IAuthServices, AuthServices>();
            
            services.AddScoped<ITransaccionServices, InvoiceServices>();


            ConfigAuthentication(services);
        }


        private void ConfigAuthentication(IServiceCollection services)
        {

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {

                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = JwtMiddleware.JWTParameters(
                    Configuration.GetValue<string>("Auth:SecretKey"),
                    Configuration.GetValue<string>("Auth:Issuer"),
                    Configuration.GetValue<string>("Auth:Audience"));
            });


        //    services.AddIdentity<User, IdentityRole>()
        //.AddEntityFrameworkStores<FEContext>()
        //.AddDefaultTokenProviders();


        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            //app.UseRouting();

            //app.UseAuthorization();
            //app.UseAuthentication();
            //app.UseMiddleware<JwtMiddleware>();
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllerRoute(
            //        name: "default",
            //        pattern: "{controller}/{action=Index}/{id?}");

            //    endpoints.MapControllers();
            //});

            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });


            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<FEContext>();
                
            }
        }

        //public static void ConfigureSqlServerContext(this IServiceCollection services, IConfiguration config)
        //{
        //    services.AddDbContext<FEContext>(a => a.UseSqlServer(config["SqlServerconection:connectionString"]));
        //}
    }
}
