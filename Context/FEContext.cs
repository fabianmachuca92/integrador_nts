﻿using Entities.Models.Auth;
using Entities.Models.Business;
using Entities.Models.General;
using Microsoft.EntityFrameworkCore;

namespace Context
{
    public class FEContext : DbContext
    {


        public FEContext(DbContextOptions options)
            : base(options)
        {
        }


        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<ccTransaccion> ccTransaccion { get; set; }
        public virtual DbSet<ccTransaccionItem> ccTransaccionItem { get; set; }
      

    }
}